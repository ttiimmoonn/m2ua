import logging
from modules.message import BaseM2UAMessage
from modules.params import ParamList
from modules.exceptions import M2uaProtocolError
from modules.params import M2uaParam, M2uaParamHeader


logger = logging.getLogger("logger")
logging.basicConfig(format=u'%(asctime)-8s %(levelname)-8s [%(module)s -> %(funcName)s:%(lineno)d] %(message)-8s',
                    level=logging.DEBUG)
logger.setLevel(logging.DEBUG)


class ASPSM(BaseM2UAMessage):
    MsgClassCode = 3
    MsgClassText = "ASPSM"

    Messages = dict()

    @classmethod
    def add_msg(cls, mclass: BaseM2UAMessage):
        cls.Messages[mclass.get_message_id()] = mclass
        return mclass

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


@ASPSM.add_msg
class AspUP(ASPSM):
    MsgTypeCode = 1
    MsgTypeText = "AspUP"

    def __init__(self, buf=None, **kwargs):
        super().__init__(**kwargs)
        self._buf = buf
        self.MsgAllowParams = [ParamList.ASPIdentifier]

    def decode(self, buf):
        self._decode_params(buf)


@ASPSM.add_msg
class AspDown(ASPSM):
    MsgTypeCode = 2
    MsgTypeText = "AspDown"

    def __init__(self):
        super().__init__()


@ASPSM.add_msg
class HeartBeat(ASPSM):
    MsgTypeCode = 3
    MsgTypeText = "HeartBeat"

    def __init__(self):
        super().__init__()


@ASPSM.add_msg
class AspUpAck(ASPSM):
    MsgTypeCode = 4
    MsgTypeText = "AspUpAck"

    def __init__(self, buf=None, **kwargs):
        super().__init__(**kwargs)
        self._buf = buf

    def encode(self):
        pass


@ASPSM.add_msg
class AspDownAck(ASPSM):
    MsgTypeCode = 5
    MsgTypeText = "AspDownAck"

    def __init__(self):
        super().__init__()


@ASPSM.add_msg
class HeartbeatAck(ASPSM):
    MsgTypeCode = 6
    MsgTypeText = "HeartbeatAck"

    def __init__(self):
        super().__init__()


if __name__ == "__main__":
    a = AspUP()
