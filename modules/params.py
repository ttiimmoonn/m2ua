import logging

from struct import Struct
from collections import namedtuple
from enum import IntEnum

from modules.exceptions import M2uaDecoderError, M2uaProtocolError

logger = logging.getLogger("logger")
logging.basicConfig(format=u'%(asctime)-8s %(levelname)-8s [%(module)s -> %(funcName)s:%(lineno)d] %(message)-8s',
                    level=logging.DEBUG)
logger.setLevel(logging.DEBUG)
# e d   codes        Description
# --------------------------------------------
# - - 0 (0x00)       Reserved
# - - 1 (0x01)       Interface Identifier (Integer)
# - - 2 (0x02)       Unused
# - - 3 (0x03)       Interface Identifier (Text)
# - + 4 (0x04)       Info String
# - - 5 (0x05)       Unused
# - - 6 (0x06)       Unused
# - + 7 (0x07)       Diagnostic Information
# - - 8 (0x08)       Interface Identifier (Integer Range)
# - + 9 (0x09)       Heartbeat Data
# - - 10 (0x0a)       Unused
# - + 11 (0x0b)       Traffic Mode Type
# - + 12 (0x0c)       Error Code
# - + 13 (0x0d)       Status Type/Information
# - - 14 (0x0e)       Unused
# - - 15 (0x0f)       Unused
# - - 16 (0x10)       Unused
# - + 17 (0x11)       ASP Identifier
# - - 18 (0x12)       Unused
# - + 19 (0x13)       Correlation Id
# - - 18-255           Reserved
# d - decoder
# e - encoder


class M2uaParamHeader:
    Header = Struct("!HH")

    @classmethod
    def param_hdr_decode(cls, buf):
        try:
            hdr_params = cls.Header.unpack_from(buf[:cls.Header.size])
            header = namedtuple('ParamHeader', 'p_tag p_len')
            header = header._make(hdr_params)
        except Exception as exp:
            logger.error("Parameter header decode error: %s\nBUFFER: %s", exp, buf.tobytes())
            raise M2uaDecoderError
        buf = buf[cls.Header.size:]
        return buf, header


class ParamList(IntEnum):
    ASPIdentifier = 17
    InterfaceIdentifierInt = 1
    InterfaceIdentifierTxt = 3

    @classmethod
    def get_name_by_id(cls, ind):
        val = cls._value2member_map_.get(ind)
        if not val:
            return None
        else:
            return val.name


class M2uaParam:
    Params = dict()

    @classmethod
    def add_param(cls, param_class):
        cls.Params[param_class.ParamTag] = param_class

    @classmethod
    def get_param_class(cls, code):
        return cls.Params.get(code)

    SupportedParams = dict()


@M2uaParam.add_param
class InterfaceIdentifierInt:
    ParamTag = 1
    InterfaceID = None

    def decode(self, buf):
        pass


@M2uaParam.add_param
class InterfaceIdentifierText:
    ParamTag = 3


@M2uaParam.add_param
class InfoString:
    ParamTag = 4
    ParamName = "Info String"
    Decoder = Struct("!I")

    def __init__(self):
        self.INFOString = None
        self.Length = None

    def __str__(self):
        out = list()
        out.append(" -> Parameter tag: {} ({})".format(self.ParamName, self.ParamTag))
        out.append("    Param length: {}".format(self.Length))
        out.append("    {}: {}".format(self.ParamName, self.INFOString))
        return "\n".join(out)

    def decode(self, header, buf):
        logger.debug("Try to decode param: tag: %d, len: %d, buf: %s", header.p_tag, header.p_len,
                     buf[:self.Decoder.size].tobytes())
        self.Length = header.p_len
        try:
            result = buf[:self.Decoder.size].decode(encoding="utf-8")
            self.INFOString = result
        except Exception as exp:
            logger.error("%s decode error. ExcInfo: %s.\nBUFFER: %s", self.ParamName, exp,
                         buf[:self.Decoder.size].tobytes())
            raise M2uaDecoderError
        return buf[self.Decoder.size:]


@M2uaParam.add_param
class DiagnosticInformation:
    ParamTag = 7
    ParamName = "Diagnostic Information"
    Decoder = Struct("!I")

    def __init__(self):
        self.DiagnosticInformation = None
        self.Length = None

    def __str__(self):
        out = list()
        out.append(" -> Parameter tag: {} ({})".format(self.ParamName, self.ParamTag))
        out.append("    Param length: {}".format(self.Length))
        out.append("    {}: {}".format(self.ParamName, self.DiagnosticInformation))
        return "\n".join(out)

    def decode(self, header, buf):
        logger.debug("Try to decode param: tag: %d, len: %d, buf: %s", header.p_tag, header.p_len,
                     buf[:self.Decoder.size].tobytes())
        self.Length = header.p_len
        try:
            result = buf[:self.Decoder.size].decode(encoding="utf-8")
            self.DiagnosticInformation = result
        except Exception as exp:
            logger.error("%s decode error. ExcInfo: %s.\nBUFFER: %s", self.ParamName, exp,
                         buf[:self.Decoder.size].tobytes())
            raise M2uaDecoderError
        return buf[self.Decoder.size:]


@M2uaParam.add_param
class InterfaceIdentifierIntRange:
    ParamTag = 8
    InterfaceID = None

    def decode(self, buf):
        pass


@M2uaParam.add_param
class Heartbeat:
    ParamTag = 9
    ParamName = "Heartbeat"
    Decoder = Struct("!I")

    def __init__(self):
        self.HeartbeatData = None
        self.Length = None

    def __str__(self):
        out = list()
        out.append(" -> Parameter tag: {} ({})".format(self.ParamName, self.ParamTag))
        out.append("    Param length: {}".format(self.Length))
        out.append("    {}: {}".format(self.ParamName, self.HeartbeatData))
        return "\n".join(out)

    def decode(self, header, buf):
        logger.debug("Try to decode param: tag: %d, len: %d, buf: %s", header.p_tag, header.p_len,
                     buf[:self.Decoder.size].tobytes())
        self.Length = header.p_len
        try:
            result = self.Decoder.unpack_from(buf[:self.Decoder.size])
            self.HeartbeatData = result[0]
        except Exception as exp:
            logger.error("%s decode error. ExcInfo: %s.\nBUFFER: %s", self.ParamName, exp,
                         buf[:self.Decoder.size].tobytes())
            raise M2uaDecoderError
        return buf[self.Decoder.size:]


@M2uaParam.add_param
class TrafficModeType:
    ParamTag = 11
    ParamName = "Traffic Mode Type"
    ValueParam = {'1': 'Override', '2': 'Load-share', '3': 'Broadcast'}
    Decoder = Struct("!I")

    def __init__(self):
        self.TrafficModeType = None
        self.TrafficModeTypeDescription = None
        self.Length = None

    def __str__(self):
        out = list()
        out.append(" -> Parameter tag: {} ({})".format(self.ParamName, self.ParamTag))
        out.append("    Param length: {}".format(self.Length))
        out.append("    {}: {}({})".format(self.ParamName, self.TrafficModeTypeDescription, self.TrafficModeType))
        return "\n".join(out)

    def decode(self, header, buf):
        logger.debug("Try to decode param: tag: %d, len: %d, buf: %s", header.p_tag, header.p_len,
                     buf[:self.Decoder.size].tobytes())
        self.Length = header.p_len
        try:
            result = self.Decoder.unpack_from(buf[:self.Decoder.size])
            if str(result[0]) in self.ValueParam:
                self.TrafficModeType = result[0]
                self.TrafficModeTypeDescription = self.ValueParam[str(result)]
            else:
                logger.error("%s invalid parameter received. Param: TrafficModeType. Receive: %s\nBUFFER: %s",
                             self.ParamName, str(result[0]), buf[:self.Decoder.size].tobytes())
                raise M2uaProtocolError
        except Exception as exp:
            logger.error("%s decode error. ExcInfo: %s.\nBUFFER: %s", self.ParamName, exp,
                         buf[:self.Decoder.size].tobytes())
            raise M2uaDecoderError
        return buf[self.Decoder.size:]


@M2uaParam.add_param
class ErrorCode:
    ParamTag = 12
    ParamName = "Error Code"
    ErrorCodes = {
                  '1': 'Invalid Version', '2': 'Invalid Interface Identifier',
                  '3': 'Unsupported Message Class', '4': 'Unsupported Message Type',
                  '5': 'Unsupported Traffic Handling Mode', '6': 'Unexpected Message',
                  '7': 'Protocol Error', '8': 'Unsupported Interface Identifier Type',
                  '9': 'Invalid Stream Identifier', '10': 'Not Used in M2UA',
                  '11': 'Not Used in M2UA', '12': 'Not Used in M2UA',
                  '13': 'Refused - Management Blocking', '14': 'ASP Identifier Required',
                  '15': 'Invalid ASP Identifier', '16': 'ASP Active for Interface Identifier(s)',
                  '17': 'Invalid Parameter Value', '18': 'Parameter Field Error',
                  '19': 'Unexpected Parameter', '20': 'Not Used in M2UA',
                  '21': 'Not Used in M2UA', '22': 'Missing Parameter'
                  }
    Decoder = Struct("!I")

    def __init__(self):
        self.ErrorCode = None
        self.ErrorDescription = None
        self.Length = None

    def __str__(self):
        out = list()
        out.append(" -> Parameter tag: {} ({})".format(self.ParamName, self.ParamTag))
        out.append("    Param length: {}".format(self.Length))
        out.append("    {}: {}({})".format(self.ParamName, self.ErrorDescription, self.ErrorCode))
        return "\n".join(out)

    def decode(self, header, buf):
        logger.debug("Try to decode param: tag: %d, len: %d, buf: %s", header.p_tag, header.p_len,
                     buf[:self.Decoder.size].tobytes())
        self.Length = header.p_len
        try:
            result = self.Decoder.unpack_from(buf[:self.Decoder.size])
            if str(result[0]) in self.ErrorCodes:
                self.ErrorCode = result[0]
                self.ErrorDescription = self.ErrorCode[str(result[0])]
            else:
                logger.error("%s invalid parameter received. Param: ErrorCodes. Receive: %s\nBUFFER: %s",
                             self.ParamName, str(result[0]), buf[:self.Decoder.size].tobytes())
                raise M2uaProtocolError
        except Exception as exp:
            logger.error("%s decode error. ExcInfo: %s.\nBUFFER: %s", self.ParamName, exp,
                         buf[:self.Decoder.size].tobytes())
            raise M2uaDecoderError
        return buf[self.Decoder.size:]


@M2uaParam.add_param
class StatusTypeAndInformation:
    ParamTag = 13
    ParamName = "Status Type/Information"
    ValueType = {'1': 'Application Server state change (AS_State_Change)', '2': 'Other'}
    ValueInformation = {
        'Application Server state change (AS_State_Change)':
            {
                '1': 'reserved',
                '2': 'Application Server Inactive (AS_Inactive)',
                '3': 'Application Server Active (AS_Active)',
                '4': 'Application Server Pending (AS_Pending)',
             },
        'Other':
            {
                '1': 'Insufficient ASP resources active in AS',
                '2': 'Alternate ASP Active',
                '3': 'ASP Failure',
            }
                        }
    Decoder = Struct("!I")

    def __init__(self):
        self.StatusType = None
        self.StatusTypeDescription = None
        self.StatusInformation = None
        self.StatusInformationDescription = None
        self.Length = None

    def __str__(self):
        out = list()
        out.append(" -> Parameter tag: {} ({})".format(self.ParamName, self.ParamTag))
        out.append("    Param length: {}".format(self.Length))
        out.append("    {}(Type): {}({})".format(self.ParamName, self.StatusTypeDescription, self.StatusType))
        out.append("    {}(Information): {}({})".format(self.ParamName,
                                                        self.StatusInformationDescription,
                                                        self.StatusInformation))
        return "\n".join(out)

    def decode(self, header, buf):
        logger.debug("Try to decode param: tag: %d, len: %d, buf: %s", header.p_tag, header.p_len,
                     buf[:self.Decoder.size].tobytes())
        self.Length = header.p_len
        try:
            result = self.Decoder.unpack_from(buf[:self.Decoder.size])
            if str(result[0]) in self.ValueType:
                self.StatusType = result[0]
                self.StatusTypeDescription = self.ValueType[str(result[0])]
                if str(result[1]) in self.ValueInformation[str(self.StatusTypeDescription)]:
                    self.StatusInformation = result[1]
                    self.StatusInformationDescription = \
                        self.ValueInformation[str(self.StatusType)][str(self.StatusInformation)]
                else:
                    logger.error("%s invalid parameter received. Param: StatusInformation. Receive: %s\nBUFFER: %s",
                                 self.ParamName, str(result[1]), buf[:self.Decoder.size].tobytes())
                    raise M2uaProtocolError
            else:
                logger.error("%s invalid parameter received. Param: StatusType. Receive: %s\nBUFFER: %s",
                             self.ParamName, str(result[0]), buf[:self.Decoder.size].tobytes())
                raise M2uaProtocolError
        except Exception as exp:
            logger.error("%s decode error. ExcInfo: %s.\nBUFFER: %s", self.ParamName, exp,
                         buf[:self.Decoder.size].tobytes())
            raise M2uaDecoderError
        return buf[self.Decoder.size:]


@M2uaParam.add_param
class ASPIdentifier:
    ParamTag = 17
    ParamName = "ASP Identifier"
    Decoder = Struct("!I")

    def __init__(self):
        self.AspInd = None
        self.Length = None

    def __str__(self):
        out = list()
        out.append(" -> Parameter tag: {} ({})".format(self.ParamName, self.ParamTag))
        out.append("    Param length: {}".format(self.Length))
        out.append("    {}: {}".format(self.ParamName, self.AspInd))
        return "\n".join(out)

    def decode(self, header, buf):
        logger.debug("Try to decode param: tag: %d, len: %d, buf: %s", header.p_tag, header.p_len,
                     buf[:self.Decoder.size].tobytes())
        self.Length = header.p_len
        try:
            result = self.Decoder.unpack_from(buf[:self.Decoder.size])
            self.AspInd = result[0]
        except Exception as exp:
            logger.error("%s decode error. ExcInfo: %s.\nBUFFER: %s", self.ParamName, exp,
                         buf[:self.Decoder.size].tobytes())
            raise M2uaDecoderError
        return buf[self.Decoder.size:]


@M2uaParam.add_param
class CorrelationId:
    ParamTag = 19
    ParamName = "Correlation Id"
    Decoder = Struct("!I")

    def __init__(self):
        self.CorrelationId = None
        self.Length = None

    def __str__(self):
        out = list()
        out.append(" -> Parameter tag: {} ({})".format(self.ParamName, self.ParamTag))
        out.append("    Param length: {}".format(self.Length))
        out.append("    {}: {}".format(self.ParamName, self.CorrelationId))
        return "\n".join(out)

    def decode(self, header, buf):
        logger.debug("Try to decode param: tag: %d, len: %d, buf: %s", header.p_tag, header.p_len,
                     buf[:self.Decoder.size].tobytes())
        self.Length = header.p_len
        try:
            result = self.Decoder.unpack_from(buf[:self.Decoder.size])
            self.CorrelationId = result[0]
        except Exception as exp:
            logger.error("%s decode error. ExcInfo: %s.\nBUFFER: %s", self.ParamName, exp,
                         buf[:self.Decoder.size].tobytes())
            raise M2uaDecoderError
        return buf[self.Decoder.size:]


if __name__ == "__main__":
    buffer = bytearray(b'\x01\x00\x03\x01\x00\x00\x00\x10\x00\x11\x00\x08\x00\x00\x00\x65')
    print(buffer)
    buffer = memoryview(buffer)
    a = M2uaParamHeader.param_hdr_decode(buffer)
    print(a)
