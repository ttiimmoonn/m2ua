import logging
from struct import Struct
from collections import namedtuple
from modules.params import M2uaParamHeader, M2uaParam
from modules.exceptions import M2uaProtocolError
from modules.params import ParamList

logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)


class BaseM2UAMessage:
    MsgClassCode = None
    MsgClassText = None
    MsgTypeCode = None
    MsgTypeText = None

    def encode(self):
        pass

    def decode(self, buf):
        pass

    def __str__(self):
        out = list()
        out.append("\nMTP 2 User Adaptation Layer")
        out.append(" Version: Release {} ({})".format(self.MsgVersion, self.MsgVersion))
        out.append(" Message Class: {} ({})".format(self.MsgClassText, self.MsgClassCode))
        out.append(" Message Type: {} ({})".format(self.MsgTypeText, self.MsgTypeCode))
        out.append(" Message Length: {}".format(self.MsgLength))
        list(map(lambda x: out.append(str(x)), self.MsgParams))
        return "\n".join(out)

    def _chk_allowed_params(self):
        msg_param_code = set([x.ParamTag for x in self.MsgParams])
        diff = msg_param_code.difference(self.MsgAllowParams)
        if diff:
            diff = list(map(lambda x: ParamList.get_name_by_id(x), diff))
            logger.error("Unexpected params: %s has been received in message: %s", diff, self)
            raise M2uaProtocolError

    def _decode_params(self, buf):
        while buf:
            buf, header = M2uaParamHeader.param_hdr_decode(buf)
            param_class = M2uaParam.get_param_class(header.p_tag)
            if not param_class:
                raise NotImplemented("Param %d isn't implemented yet.")
            param = param_class()
            buf = param.decode(header, buf)
            self.MsgParams.append(param)
        # check for unexpected params in message
        self._chk_allowed_params()

    def __init__(self, m_header=None):
        self.MsgVersion = m_header.version if m_header else None
        self.MsgLength = m_header.length if m_header else None
        self.MsgParams = list()
        self.MsgAllowParams = set()

    @classmethod
    def get_message_id(self):
        return self.MsgClassCode, self.MsgTypeCode


class M2uaHeader:
    ComMsgHeader = Struct("!BBBBI")

    @classmethod
    def common_hdr_decode(cls, buf):
        """
        Decode common M2UA header
        :param buf: memoryview object
        :return: namedtuple
        """
        hdr_params = cls.ComMsgHeader.unpack_from(buf[:cls.ComMsgHeader.size])
        header = namedtuple('CommonMsgHeader', 'version spare msg_class msg_type length')
        header = header._make(hdr_params)
        buf = buf[cls.ComMsgHeader.size:]
        return buf, header


if __name__ == "__main__":
    pass


# from struct import Struct
# from collections import namedtuple
# from modules.aspsm import ASPSM
# import logging
#
#
# logger = logging.getLogger("logger")
# logging.basicConfig(format=u'%(asctime)-8s %(levelname)-8s [%(module)s -> %(funcName)s:%(lineno)d] %(message)-8s',
#                     level=logging.DEBUG)
# logger.setLevel(logging.DEBUG)
#
#
#
#
#
#
# class Message(object):
#     # 3.1  Common Message Header
#     #     0                   1                   2                   3
#     #     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
#     #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#     #    |    Version    |     Spare     | Message Class | Message Type  |
#     #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#     #    |                        Message Length                         |
#     #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
#     ComMsgHeader = Struct("!BBBBI")
#
#     MsgClass = {
#         ASPSM.ClassCode: ASPSM
#     }
#
#     @classmethod
#     def add_msg(cls, msg):
#         cls.MsgClass[(msg.ClassCode, msg.TypeCode)] = msg
#         logger.warning("%s", cls.MsgClass)
#
#     @classmethod
#     def common_hdr_decode(cls, buf):
#         """
#         Decode common M2UA header
#         :param buf: memoryview object
#         :return: namedtuple
#         """
#         hdr_params = cls.ComMsgHeader.unpack_from(buf[:cls.ComMsgHeader.size])
#         header = namedtuple('CommonMsgHeader', 'version spare msg_class msg_type length')
#         header = header._make(hdr_params)
#         buf = buf[cls.ComMsgHeader.size:]
#         return header
#
#     def __init__(self, msg_header, buf):
#         msg_class = self.MsgClass.get(msg_header.msg_class)
#         if not msg_class:
#             raise NotImplemented("Message class: %s hasn't been implemented yet." % msg_header.msg_class)
#         msg = msg_class.get_msg_class(msg_header.msg_type)
#         if not msg:
#             raise NotImplemented("Message type: %s hasn't been implemented yet." % msg_header.msg_type)
#
#
# #
# # class Message:
# #     ClassMsgMap = {
# #         3: "test"
# #     }
# #     ComHeader = Struct("!BBBBI")
# #     # 3.1  Common Message Header
# #     #     0                   1                   2                   3
# #     #     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
# #     #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# #     #    |    Version    |     Spare     | Message Class | Message Type  |
# #     #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# #     #    |                        Message Length                         |
# #     #    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# #
# #     def __init__(self, buf=bytearray()):
# #         self._buf = buf
# #         self._m_buf = memoryview(buf) if buf else None
# #
# #         # TODO: сделать обработку исключений
# #         header_params = self.ComHeader.unpack_from(self._m_buf[:self.ComHeader.size])
# #         self._version, _, self._m_class, self._m_type, self._m_len = header_params
# #         self._update_m_buf(self.ComHeader.size)
# #
# #         # Class message properties
# #         self.ClassCode = None
# #         self.ClassShortTxt = None
# #         self.ClassFullTxt = None
# #
# #         # Type message properties
# #         self.TypeTxt = None
# #         self.TypeCode = None
# #
# #         # Message Parameters
# #         self.Params = CParam.get_params(self._m_buf[self.ComHeader.size:])
# #         # TODO: обработать ситуацию отсутствующего класса сообщений!
# #         Message.ClassMsgMap.get(self._m_class).__init__(self, self._m_type, self.Params)
# #
# #     def __str__(self):
# #         output = list()
# #         output.append("")
# #         output.append("Version: {}".format(self._version))
# #         output.append("Message Class: {} ({})".format(self.ClassFullTxt, self._m_class))
# #         output.append("Message Type: {} ({})".format(self.TypeTxt, self._m_type))
# #         output.append("Message Len: {}".format(self._m_len))
# #         return "\n   ".join(output)
# #
# #     def _update_m_buf(self, size):
# #         self._m_buf = self._m_buf[size:]
# #
# #     def _encode_common_header(self):
# #         pass
#
# if __name__ == "__main__":
#     buffer = bytearray(b'\x01\x00\x03\x01\x00\x00\x00\x10\x00\x11\x00\x08\x00\x00\x00\x65'
#                        b'\x00\x04\x00\x0dtest_data\x00\x00\x00')
#     buffer = memoryview(buffer)
#     msg_hdr = Message.common_hdr_decode(buffer)
#     msg = Message(msg_hdr, buffer)
