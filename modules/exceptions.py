class M2uaException(Exception):
    pass


class M2uaProtocolError(M2uaException):
    pass


class M2uaDecoderError(M2uaException):
    pass
