import sctp
import socket
import logging
from threading import Thread
from enum import Enum, auto

from modules.aspsm import ASPSM
from modules.message import M2uaHeader
from modules.exceptions import M2uaException

logger = logging.getLogger("logger")
logging.basicConfig(format=u'%(asctime)-8s %(levelname)-8s [%(module)s -> %(funcName)s:%(lineno)d] %(message)-8s',
                    level=logging.DEBUG)
logger.setLevel(logging.DEBUG)

MESSAGES = [ASPSM]
MAX_CONN = 10
BUF_SIZE = 4096


class CoreStates(Enum):
    INIT = auto()
    START = auto()
    STOP = auto()


class M2UACore:

    Messages = {}

    def __new__(cls, *args, **kwargs):
        if not cls.Messages:
            list(map(lambda x: cls.Messages.update(x), [x.Messages for x in MESSAGES]))
        obj = super(M2UACore, cls).__new__(cls)
        return obj

    def __init__(self, **kwargs):
        self.State = CoreStates.INIT
        self.BindAddr = kwargs.get("bind_addr")
        self.Socket = sctp.sctpsocket_tcp(socket.AF_INET)

        self.Connections = set()

    # Private methods

    def _add_connection(self, sock):
        self.Connections.add(sock)

    def _del_connection(self, sock):
        try:
            self.Connections.remove(sock)
        except KeyError:
            pass

    def _start_transport(self):
        try:
            self.Socket.bind(self.BindAddr)
        except Exception as exp:
            logger.error("Bind failed: %s", exp)
            return False
        self.Socket.listen(MAX_CONN)
        return True

    def _handle_incoming_message(self, buf, cl_address):
        buf = memoryview(buf)
        try:
            buf, m2ua_header = M2uaHeader.common_hdr_decode(buf)
        except Exception as exp:
            logger.warning("M2ua common header is wrong. Error: %s", exp)
            logger.exception(exp)
            return False
        msg_class = self.Messages.get((m2ua_header.msg_class, m2ua_header.msg_type))
        if not msg_class:
            logger.warning("Class for (%d,%d) not implemented yet")
            return False
        try:
            m2ua_msg = msg_class(buf=buf, m_header=m2ua_header)
            m2ua_msg.decode(buf)
        except M2uaException:
            logger.info("Message has been skipped.")
            return False
        except NotImplemented:
            logger.error("Handler for message hasn't implemented yet!")
            return False
        return self.handle_incoming_message(m2ua_msg, cl_address)

    def _handle_socket_data(self, sock, address):
        logger.debug("Start data handler for %s:%d", *address)
        try:
            while True:
                cl_address, flags, data, notif = sock.sctp_recv(BUF_SIZE)
                if not data:
                    return
                if not self._handle_incoming_message(data, cl_address):
                    return
        except OSError as exp:
            if self.State is CoreStates.STOP:
                return
            else:
                logging.exception(exp)
                return
        finally:
            sock.close()
            logger.debug("Connection to: %s:%d has been closed.", *address)

    def _accept_connections(self):
        logger.info("Start listening on %s:%d", *self.BindAddr)
        while True:
            sock, address = self.Socket.accept()
            self._add_connection(sock)
            logger.debug("Accept connection from %s:%d", *address)
            new_thread = Thread(target=self._handle_socket_data, args=(sock, address,))
            new_thread.start()

    # Public methods
    def handle_incoming_message(self, m2ua_msg, cl_address):
        logger.info("Receive m2ua message from %s:%d:%s", cl_address[0], cl_address[1], m2ua_msg)

    def start(self):
        if not self._start_transport():
            return False
        self.State = CoreStates.START
        self._accept_connections()

    def stop(self):
        self.State = CoreStates.STOP
        list(map(lambda x: x.close(), self.Connections))


if __name__ == "__main__":
    new_core = M2UACore(bind_addr=("192.168.116.180", 10000))
    try:
        new_core.start()
    except KeyboardInterrupt:
        new_core.stop()
