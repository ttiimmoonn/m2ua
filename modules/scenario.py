import logging
import pprint
from modules.core import M2UACore


logger = logging.getLogger("logger")
logging.basicConfig(format=u'%(asctime)-8s %(levelname)-8s [%(module)s -> %(funcName)s:%(lineno)d] %(message)-8s',
                    level=logging.DEBUG)
logger.setLevel(logging.DEBUG)


class ScenarioCmd:
    RecvMessage = "RecvMessage"
    SendMessage = "SendMessage"


class Scenario(M2UACore):
    PrettyPrint = pprint.PrettyPrinter(indent=1, width=1)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.Scenario = kwargs.get("scenario")
        self.MainScenario = self._main_scenario()
        self.ExpectedEvent = None

    def _main_scenario(self):
        for item in self.Scenario.get("MainScenario"):
            yield item

    def _compare_messages(self, m2ua_msg, exp_message):
        # TODO реализовать сравнение сообщений
        return True

    def _handle_unexpected(self, m2ua_msg):
        logger.error("Receive unexpected message.\n::Expected message:\n%s\n::Received message: %s",
                     self.PrettyPrint.pformat(self.ExpectedEvent), m2ua_msg)

    def _check_for_send(self):
        try:
            if not self.ExpectedEvent:
                self.ExpectedEvent = next(self.MainScenario)
        except StopIteration:
            # TODO: сценарий завершён, нужно обрать внимание на обработку данного события
            return False
        event = self.ExpectedEvent.get(ScenarioCmd.SendMessage)
        if not event:
            # We have nothing for send. Return...
            return

        msg_class_code = event.get("MsgClassCode")
        msg_type_code = event.get("MsgTypeCode")
        msg_class = self.Messages.get((msg_class_code, msg_type_code))
        logger.info("Send message!!")
        msg = msg_class()
        logger.info("New object for send! %s", msg)

    def apply_incoming_message(self, m2ua_msg):
        try:
            if not self.ExpectedEvent:
                self.ExpectedEvent = next(self.MainScenario)
        except StopIteration:
            logger.error("Receive unexpected message to completed M2UA script. %s", m2ua_msg)
            return False
        event = self.ExpectedEvent.get(ScenarioCmd.RecvMessage)
        if not event:
            self._handle_unexpected(m2ua_msg)
            return False
        if not self._compare_messages(m2ua_msg, event):
            # TODO: Нужно придумать ошибку при compare
            return False
        else:
            # Clear expected event!
            self.ExpectedEvent = None
        # Is there anything for send?
        self._check_for_send()

    def handle_incoming_message(self, m2ua_msg, cl_address):
        logging.debug("Receive new message from %s:%d%s", cl_address[0], cl_address[1], m2ua_msg)
        self.apply_incoming_message(m2ua_msg)
        return True


if __name__ == "__main__":
    test_scenario = {
        "MainScenario": [
            {
                "RecvMessage": {
                    "MsgClassCode": 3,
                    "MsgTypeCode": 3,
                    "MsgClassText": "ASPSM",
                    "MsgTypeText": "AspUP",
                    "Params": [
                        {
                            "ParamTag": 17,
                            "AspID": 89895
                        }
                    ]
                }
            },
            {
                "SendMessage": {
                    "MsgClassCode": 3,
                    "MsgTypeCode": 4
                }
            }
        ]
    }

    new_scenario = Scenario(bind_addr=("192.168.116.180", 10000), scenario=test_scenario)
    try:
        new_scenario.start()
    except KeyboardInterrupt:
        new_scenario.stop()

